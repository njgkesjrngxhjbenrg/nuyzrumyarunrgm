FROM golang:1.20
WORKDIR /app
COPY src/go.mod src/go.sum ./
RUN go mod download
COPY src /app
RUN CGO_ENABLED=0 GOOS=linux go build -tags go_tarantool_ssl_disable .
CMD ["/app/bot"]
