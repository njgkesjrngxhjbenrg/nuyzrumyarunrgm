В вакансии есть слово Go, так что из предложенных языков буду писать на нём. Поскольку в вакансии также есть слово Tarantool, для хранения данных используется он. В вакансии также упоминается Docker, так что будет использован Docker (делать вид что я знаю что такое Kubernetes я не буду, так что будет магия docker-compose).

Для запуска `chmod a+w data` (оказывается git не умеет такое сохранять), написать
```
TELEGRAM_TOKEN=[токен]
```
в файл bot.env, и `docker-compose up`.

