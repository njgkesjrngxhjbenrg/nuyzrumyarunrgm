box.cfg{
	listen = '3301',
	work_dir = '/opt/tarantool/data',
	memtx_dir = '.',
	wal_dir = '.'
}
box.schema.user.passwd('pass')

passwords = box.schema.space.create('passwords', { if_not_exists=true })
passwords:format({{ name='uid', type='number'}, { name='service', type='string'}, { name='password', type='string'}})

passwords:create_index('pk', { if_not_exists=true, unique=true, parts={{ field='uid', type='number'}, { field='service', type='string'}}})
