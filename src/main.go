package main

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/tarantool/go-tarantool"
	"log"
	"os"
	"strings"
	"sync"
	"time"
)

var helpString = `/set <service name> <password>
/get <service name>
/del <service name>
messages that contain passwords are autoremoved after 10 seconds`
var Bot *tgbotapi.BotAPI
var BotMutex sync.Mutex
var DBConn *tarantool.Connection

type DBEntry struct {
	Uid      int
	Service  string
	Password string
}

func waitAndDel(msgid int, uid int64) {
	time.Sleep(10 * time.Second)
	BotMutex.Lock()
	msg := tgbotapi.NewDeleteMessage(uid, msgid)
	_, err := Bot.Request(msg)
	if err != nil {
		log.Printf("failed to delete message", err)
	}
	BotMutex.Unlock()
}
func reply(uid int64, text string) int {
	BotMutex.Lock()
	msg := tgbotapi.NewMessage(uid, text)
	ret, err := Bot.Send(msg)
	if err != nil {
		log.Printf("failed to send message", err)
	}
	BotMutex.Unlock()
	return ret.MessageID
}
func handleSet(args string, uid int64) {
	// assume there are no spaces in the service name (spaces in the password are legal)
	service, password, found := strings.Cut(args, " ")
	if !found {
		reply(uid, "syntax: /set <service name> <password>")
		return
	}
	req := tarantool.NewUpsertRequest("passwords").
		Tuple([]interface{}{uid, service, password}).
		Operations(tarantool.NewOperations().Assign(2, password))
	_, err := DBConn.Do(req).Get()
	if err != nil {
		reply(uid, "Error: "+err.Error())
	} else {
		reply(uid, "OK")
	}
}
func handleGet(service string, uid int64) {
	if service == "" || strings.Contains(service, " ") {
		reply(uid, "syntax: /get <service name>")
		return
	}
	req := tarantool.NewSelectRequest("passwords").
		Index("pk").
		Limit(1).
		Key([]interface{}{uid, service})
	var row []DBEntry
	err := DBConn.Do(req).GetTyped(&row)
	if err != nil {
		reply(uid, "Error: "+err.Error())
		return
	}
	if len(row) != 1 {
		reply(uid, "No such password")
	} else {
		mid := reply(uid, row[0].Password)
		waitAndDel(mid, uid)
	}
}
func handleDel(service string, uid int64) {
	if service == "" || strings.Contains(service, " ") {
		reply(uid, "syntax: /del <service name>")
		return
	}
	req := tarantool.NewDeleteRequest("passwords").
		Index("pk").
		Key([]interface{}{uid, service})
	var row []DBEntry
	err := DBConn.Do(req).GetTyped(&row)
	if err != nil {
		reply(uid, "Error: "+err.Error())
		return
	}
	if len(row) == 1 {
		reply(uid, "OK")
	} else {
		reply(uid, "No such password")
	}
}
func main() {
	opts := tarantool.Opts{
		User:      "admin",
		Pass:      "pass",
		Reconnect: 1 * time.Second,
	}
	var err error
	DBConn, err = tarantool.Connect("tarantool:3301", opts)
	if err != nil {
		log.Panic(err)
	}
	Bot, err = tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_TOKEN"))
	if err != nil {
		log.Panic(err)
	}
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates := Bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil || update.Message.From == nil {
			continue
		}
		if !update.Message.IsCommand() {
			continue
		}
		go func(msg *tgbotapi.Message) {
			cmd := msg.Command()
			args := msg.CommandArguments()
			uid := msg.From.ID
			log.Printf("uid %d cmd %s", uid, cmd)
			if cmd == "set" {
				handleSet(args, uid)
				waitAndDel(msg.MessageID, uid)
			} else if cmd == "get" {
				handleGet(args, uid)
			} else if cmd == "del" {
				handleDel(args, uid)
			} else {
				reply(uid, helpString)
			}
		}(update.Message)
	}
}
